
//retrieve all json in folder and output their respective transcripts into a .txt

const path = require('path');
const fs = require('fs');

//joining path of directory 
const directoryPath = path.join(__dirname, 'json');


//passsing directoryPath and callback function
fs.readdir(directoryPath, function (err, files) {
    //handling error
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    } 
    //listing all files using forEach
    files.forEach(function (file) {
        //do whatever you want to do with the file
        //console.log(file); 
        //load JSON
         fs.readFile(`json/${file}`, (err, data) => {
		    if (err) throw err;
		    let contents = JSON.parse(data).results;
		    let lines = [];
		    contents.forEach((result,idx)=>{
		    	lines.push(result.alternatives[0].transcript);
		    	//write to txt 
		    	//console.log(algo);
		        fs.appendFileSync('txt/'+file.substring(0, file.length-5)+'.txt', 
		        	result.alternatives[0].transcript.trim()+"\n", function (err) {
				  if (err) return console.log(err);
				  
				});
		    });
		  
		});

    });
});





