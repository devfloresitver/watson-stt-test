#!/bin/bash

for file in $pwd/json*.flac; do
echo "${file##*/}"
curl -X POST -u "apikey:API_KEY" \
--header "Content-Type: audio/flac" \
--data-binary @${file##*/} \
"https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?smart_formatting=true?model=ja-JP_NarrowbandModel" > ${file##*/}.json
done

#node main.js